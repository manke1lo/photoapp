package ie.wit.assignment01.main

import android.app.Application
import ie.wit.assignment01.models.PhotoshareJSONStore
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import ie.wit.assignment01.models.PhotoshareStore as PhotoshareStore1

class MainApp : Application(), AnkoLogger
{
    lateinit var events : PhotoshareStore1

    override fun onCreate()
    {
        super.onCreate()
        events = PhotoshareJSONStore(applicationContext)
        info("Photoshare started")
    }
}