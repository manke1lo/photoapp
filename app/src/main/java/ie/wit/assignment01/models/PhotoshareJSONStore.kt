package ie.wit.assignment01.models

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import ie.wit.assignment01.helpers.exists
import org.jetbrains.anko.AnkoLogger
import ie.wit.assignment01.helpers.*
import org.jetbrains.anko.info
import java.util.*

val JSON_FILE = "events.json"
val gsonBuilder = GsonBuilder().setPrettyPrinting().create()
val listType = object : TypeToken<ArrayList<PhotoshareModel>>() {}.type

fun generateRandomId(): Long {
    return Random().nextLong()
}

class PhotoshareJSONStore : PhotoshareStore, AnkoLogger {

    val context: Context
    var events = mutableListOf<PhotoshareModel>()

    constructor (context: Context) {
        this.context = context
        if (exists(context, JSON_FILE)) {
            deserialize()
        }
    }

    override fun findAll(): MutableList<PhotoshareModel> {
        return events
    }

    override fun create(event: PhotoshareModel) {
        event.id = generateRandomId()
        events.add(event)
        serialize()
    }


    override fun update(event: PhotoshareModel)
    {
        var foundEvent:PhotoshareModel?=events.find{p->p.id==event.id}
        if(foundEvent!=null)
        {
            foundEvent.name=event.name
            foundEvent.start=event.start
            foundEvent.end=event.end
            foundEvent.image = event.image
            foundEvent.endTime = event.endTime
            foundEvent.startTime = event.startTime
            serialize()
        }
    }

    override fun edit(event:PhotoshareModel): PhotoshareModel? {
        var foundEvent:PhotoshareModel?=events.find{p->p.id==event.id}
        return foundEvent
    }

    override fun delete(event:PhotoshareModel)
    {
        events.remove(event)
        serialize()
    }


    private fun serialize() {
        events.forEach{info("${it}")}

        val jsonString = gsonBuilder.toJson(events, listType)
        write(context, JSON_FILE, jsonString)
    }

    private fun deserialize() {
        val jsonString = read(context, JSON_FILE)
        events = Gson().fromJson(jsonString, listType)
    }
}