package ie.wit.assignment01.models

interface PhotoshareStore {
  fun findAll(): List<PhotoshareModel>
  fun create(event: PhotoshareModel)
  fun update(event:PhotoshareModel)
  fun edit(event:PhotoshareModel): PhotoshareModel?
  fun delete(event: PhotoshareModel)
}