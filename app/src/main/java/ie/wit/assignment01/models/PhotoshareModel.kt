package ie.wit.assignment01.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PhotoshareModel(var id:Long=0,
                           var name: String = "",
                           var start: String = "",
                           var startTime: String = "",
                           var end: String = "",
                           var endTime: String = "",
                           var image: String="") : Parcelable
