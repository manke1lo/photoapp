package ie.wit.assignment01.models

import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

var lastId=0L

internal fun getId(): Long {
    return lastId++
}

class PhotoshareMemStore: PhotoshareStore, AnkoLogger {

    val events = ArrayList<PhotoshareModel>()

    override fun findAll():List<PhotoshareModel>{
        return events
    }
    
    override fun create(event:PhotoshareModel){
        event.id=getId()
        events.add(event)
        logAll()
    }

    override fun update(event:PhotoshareModel)
    {
        var foundEvent:PhotoshareModel?=events.find{p->p.id==event.id}
        if(foundEvent!=null)
        {
            foundEvent.name=event.name
            foundEvent.start=event.start
            foundEvent.end=event.end
            foundEvent.image = event.image
            foundEvent.endTime = event.endTime
            foundEvent.startTime = event.startTime
            logAll()
        }
    }

   override fun edit(event:PhotoshareModel): PhotoshareModel? {
        var foundEvent:PhotoshareModel?=events.find{p->p.id==event.id}
        return foundEvent
    }

    fun logAll()
    {
        events.forEach{info("${it}")}
    }

    override fun delete(event:PhotoshareModel)
    {
        events.remove(event)
    }

}