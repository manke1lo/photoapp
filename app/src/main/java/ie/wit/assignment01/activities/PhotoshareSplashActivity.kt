package ie.wit.assignment01.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.os.Handler
import ie.wit.assignment01.R

class PhotoshareSplashActivity: AppCompatActivity()
{
    val SPLASH_TIME_OUT:Long = 2000

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            startActivity(Intent(this, PhotoshareListActivity::class.java))
            finish()
        }, SPLASH_TIME_OUT)
    }

}