package ie.wit.assignment01.activities

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ie.wit.assignment01.R
import ie.wit.assignment01.helpers.readImageFromPath
import ie.wit.assignment01.models.PhotoshareModel
import kotlinx.android.synthetic.main.card_photoshare.view.*


interface DeleteListener {
    fun onDeleteClick(event:PhotoshareModel)
}

interface PhotoshareListener {
    fun onPhotoshareClick(event: PhotoshareModel)

}

class PhotoshareAdapter constructor(private var events: List<PhotoshareModel>,
                                    private val deleteListener:DeleteListener,
                                    private val listener: PhotoshareListener): RecyclerView.Adapter<PhotoshareAdapter.MainHolder>()
{

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainHolder
    {
        return MainHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_photoshare, parent, false))
    }

    override fun onBindViewHolder(holder: MainHolder, position: Int)
    {
        val event = events[holder.adapterPosition]
        holder.bind(event, listener, deleteListener)
    }

    override fun getItemCount(): Int = events.size

    class MainHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView)
    {

        fun bind(event: PhotoshareModel, listener:PhotoshareListener, deleteListener: DeleteListener)
        {
            itemView.name.text = event.name
            itemView.start.text = event.start
            itemView.end.text = event.end
            itemView.imageIcon.setImageBitmap(readImageFromPath(itemView.context, event.image))
            itemView.delete_btn.setOnClickListener{deleteListener.onDeleteClick(event)}
            itemView.setOnClickListener{listener.onPhotoshareClick(event)}
        }
    }
}
