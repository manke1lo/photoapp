package ie.wit.assignment01.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.squareup.picasso.Picasso
import ie.wit.assignment01.R
import ie.wit.assignment01.R.*
import ie.wit.assignment01.helpers.readImage
import ie.wit.assignment01.helpers.readImageFromPath
import ie.wit.assignment01.helpers.showImagePicker
import ie.wit.assignment01.main.MainApp
import ie.wit.assignment01.models.PhotoshareModel
import kotlinx.android.synthetic.main.activity_photo_upload.*
import kotlinx.android.synthetic.main.activity_photoshare.*
import kotlinx.android.synthetic.main.activity_photoshare.end
import kotlinx.android.synthetic.main.activity_photoshare.name
import kotlinx.android.synthetic.main.activity_photoshare.start
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.toast
import java.util.Calendar

class PhotoshareActivity : AppCompatActivity(), AnkoLogger
{

    var event = PhotoshareModel()
    lateinit var app : MainApp
    val IMAGE_REQUEST = 1

    override fun onCreate(savedInstanceState: Bundle?)
    {
        //variables to store the selected date and time
        val myCal = Calendar.getInstance()
        val year = myCal.get(Calendar.YEAR)
        val month = myCal.get(Calendar.MONTH)
        val day = myCal.get(Calendar.DAY_OF_MONTH)
        val hour = myCal.get(Calendar.HOUR_OF_DAY)
        val minute = myCal.get(Calendar.MINUTE)

        super.onCreate(savedInstanceState)
        setContentView(layout.activity_photoshare)

        toolbarAdd.title = title
        setSupportActionBar(toolbarAdd)

        app = application as MainApp
        var edit = false


        if (intent.hasExtra("event_edit")) {
            edit = true
            event = intent.extras?.getParcelable("event_edit")!!
            name.setText(event.name)
            start.setText(event.start)
            end.setText(event.end)
            startTime.setText(event.startTime)
            endTime.setText(event.endTime)
            //eventImage.setImageBitmap(readImageFromPath(this, event.image))
            val uri = Uri.parse(event.image)
            if (uri != null) {
                Picasso.with(this)
                        .load(uri)
                        .placeholder(mipmap.ic_launcher)
                        .error(mipmap.ic_launcher)
                        .fit()
                        .into(eventImage)
            }
            if(uri==null){
                event.image = mipmap.ic_launcher_round.toString()
                Picasso.with(this)
                    .load(mipmap.ic_launcher_round)
                    .placeholder(mipmap.ic_launcher)
                    .error(mipmap.ic_launcher)
                    .fit()
                    .into(eventImage)
                addImageBtn.setText(string.changeEventImage)
            }
            createEvent.setText(string.save_event)
            if(event.image != null)
            {
                addImageBtn.setText(string.changeEventImage)
            }
            createEvent.setText(string.save_event)
        }


        start.setOnClickListener {
            //when the start date text area is clicked, the date picker dialog and time picker dialog are created
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener {view, year, month, dayOfMonth ->
                // Display Selected date in textbox
                start.setText("" + dayOfMonth + "/" + (month+1) + "/" + year)
            }, year, month, day)
            val tpd = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener {view, hourOfDay, minute ->
                var time = String.format("$hourOfDay:%02d", minute)
                startTime.setText(time)
            }, hour, minute, true)

            //cannot select dates before current date
            dpd.datePicker.minDate = System.currentTimeMillis() - 1000
            //shows the dialog boxes
            tpd.show()
            dpd.show()
        }

        end.setOnClickListener {
            //when the start date text area is clicked, the date picker dialog and time picker dialog are created
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener {view, year, month, dayOfMonth ->
                // Display Selected date in textbox
                end.setText("" + dayOfMonth + "/" + (month+1) + "/" + year)
            }, year, month, day)
            val tpd = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener {view, hourOfDay, minute ->
                var time = String.format("$hourOfDay:%02d", minute)
                endTime.setText(time)
            }, hour, minute, true)

            //cannot select dates before current date
            dpd.datePicker.minDate = System.currentTimeMillis() - 1000
            //shows the dialog boxes
            tpd.show()
            dpd.show()
        }

        createEvent.setOnClickListener()
        {
            event.name = name.text.toString()
            event.start = start.text.toString()
            event.end = end.text.toString()
            event.startTime = startTime.text.toString()
            event.endTime = endTime.text.toString()

            if (event.name.isNotEmpty())
            {
                if(edit)
                {
                    app.events.update(event.copy())
                }
                else {
                    app.events.create(event.copy())
                }
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
            else {
                toast(string.enterEventDetails)
            }
        }

        addImageBtn.setOnClickListener {
            showImagePicker(this, IMAGE_REQUEST)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            IMAGE_REQUEST -> {
                if (data != null) {
                    event.image = data.getData().toString()
                    Picasso.with(this)
                            .load(data.data)
                            .placeholder(mipmap.ic_launcher)
                            .error(mipmap.ic_launcher)
                            .fit()
                            .into(eventImage)
                    addImageBtn.setText(string.changeEventImage)
                }
                if(data==null){
                    event.image = mipmap.ic_launcher_round.toString()
                    Picasso.with(this)
                        .load(mipmap.ic_launcher_round)
                        .placeholder(mipmap.ic_launcher)
                        .error(mipmap.ic_launcher)
                        .fit()
                        .into(eventImage)
                    addImageBtn.setText(string.changeEventImage)
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_cancel, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            id.cancel_btn -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
