package ie.wit.assignment01.activities

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import ie.wit.assignment01.R
import ie.wit.assignment01.helpers.*
import ie.wit.assignment01.main.MainApp
import ie.wit.assignment01.models.PhotoshareModel
import kotlinx.android.synthetic.main.activity_photo_upload.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.intentFor
import java.text.SimpleDateFormat
import java.util.*
import android.os.Build
import android.app.AlertDialog
import ie.wit.assignment01.dropbox.DropboxClient
import ie.wit.assignment01.dropbox.URI_to_Path
import android.widget.ImageView
import com.squareup.picasso.Picasso
import ie.wit.assignment01.dropbox.UploadTask
import org.jetbrains.anko.toast
import java.io.File


class PhotoUpload : AppCompatActivity(), AnkoLogger {

    private val IMAGE_REQUEST_GALLERY = 1
    private val IMAGE_REQUEST_CAMERA = 2
    private val EDIT = 3
    private val PERMISSION_CODE = 4711
    private val GET_TOKEN = 8945

    private lateinit var tempToken: String
    var nullToken: Boolean = true

    var event = PhotoshareModel()
    var image_uri: Uri? = null
    var icon_uri: Uri? = null
    lateinit var app: MainApp

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_upload)
        app = application as MainApp

        toolbarEdit_Settings.title = title
        setSupportActionBar(toolbarEdit_Settings)


        if (intent.hasExtra("event_edit")) {
            event = intent.extras?.getParcelable("event_edit")!!
            //eventImageIcon.setImageBitmap(readImageFromPath(this, event.image))
            icon_uri = Uri.parse(event.image)
            changeEventImage()
            names.setText(event.name)
            setTimeRemaining()
        }

        loadToken(intent)

        chooseImage.setOnClickListener {
            showImagePicker(this, IMAGE_REQUEST_GALLERY)
        }

        takePicture.setOnClickListener {
            handlePermissions()
        }
    }

    fun handlePermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //checks if user has camera and external storage permissions
            if (checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED) {
                val permission =
                        arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                requestPermissions(permission, PERMISSION_CODE)
            }
            else {
                launchCamera()
            }
        }
        else {
            launchCamera()
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        //called when user presses allow or deny from permission request popup
        when (requestCode) {
            PERMISSION_CODE -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    launchCamera()
                }
            }
        }
    }

    fun launchCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "Event Picture")
        image_uri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, IMAGE_REQUEST_CAMERA)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_settings_edit, menu)
        return super.onCreateOptionsMenu(menu)
    }

    //when 'Edit Event' is selected
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        //copy of the event in its current state
        var editedEvent = app.events.edit(event)

        when (item?.itemId) {
            R.id.edit_btn -> {
                //starts activity with editedEvent so that the PhotoshareActivity page reflects the most recent edits
                startActivityForResult(
                    intentFor<PhotoshareActivity>().putExtra(
                        "event_edit",
                        editedEvent
                    ), EDIT
                )
            }
            R.id.item_settings -> {
                startActivityForResult(intentFor<SettingsActivity>(), GET_TOKEN)
            }

            R.id.back_btn -> {
                startActivityForResult(intentFor<PhotoshareListActivity>(), GET_TOKEN)
            }
        }
            return super.onOptionsItemSelected(item)
        }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {

                IMAGE_REQUEST_CAMERA -> {
                    info("camera activity finished")
                    buildDialog()
                }

                IMAGE_REQUEST_GALLERY -> {
                    super.onActivityResult(requestCode, resultCode, data)
                    info("checking if gallery data is null")
                    if (data != null) {
                        info("gallery data is not null")
                        image_uri = data.data
                        buildDialog()
                    }
                }

                EDIT -> {
                    info("event edited")
                    if (data != null) {
                        var editedEvent = app.events.edit(event)
                        if (editedEvent != null) {
                            event = editedEvent
                            names.setText(editedEvent.name)
                            //eventImageIcon.setImageBitmap(readImageFromPath(this, editedEvent.image))
                            icon_uri = Uri.parse(editedEvent.image)
                            setTimeRemaining()
                            changeEventImage()
                        }
                    }
                }

                GET_TOKEN -> {
                    loadToken(data)
                }

            }
        }
    }

    fun changeEventImage() {
        if (icon_uri != null) {
            Picasso.with(this)
                    .load(icon_uri)
                    .fit()
                    .into(eventImageIcon)
        }
    }

    private fun setTimeRemaining() {
        val dateEnd = SimpleDateFormat("dd/MM/yyyyHH:mm").parse(event.end + event.endTime)
        val dateStart = SimpleDateFormat("dd/MM/yyyyHH:mm").parse(event.start + event.startTime)
        val curDate = Date()
        val diff = dateEnd.time - curDate.time
        val startDiff = dateStart.time - curDate.time
        if (diff > 0 && startDiff < 0) {
            info("event happening current date $curDate")
            val minutes = diff / (60 * 1000) % 60
            val hours = diff / (60 * 60 * 1000) % 24
            val days = diff / (24 * 60 * 60 * 1000)
            timeRemaining.setText("$days days $hours hours $minutes minutes remaining")
        }
        else {
            info("event not currently happening")
            finish()
        }
    }

    private fun buildDialog() {
        val image = ImageView(this)
        image.setImageURI(image_uri)
        val mBuilder = AlertDialog.Builder(this)
        mBuilder.setTitle("Event Picture")
        mBuilder.setView(image)
        //creates an onClick listener for "Upload to Dropbox" button
        mBuilder.setPositiveButton("Upload to Dropbox") {dialog, which ->
            if (!nullToken) {
                upload()
            }
            else {
                toast("Sign in to Dropbox in Settings")
            }
        }
        //creates an onClick listener for "Cancel" button
        mBuilder.setNeutralButton("Cancel") {dialog, which ->
            dialog.cancel()
        }
        val mDialog = mBuilder.create()
        mDialog.show()
    }

    private fun upload() {
        val file = File(URI_to_Path.getPath(application, image_uri))
        if (file != null) {
            var uploadTask = UploadTask(DropboxClient.getClient(tempToken), file, applicationContext, event.name)
            uploadTask.execute()
            info("file uploaded")
        }
    }

    private fun loadToken(intent: Intent?) {
        if(intent != null && intent.hasExtra("Token")) {
            val accessToken: String? = intent.extras?.getString("Token")
            if (accessToken != null) {
                tempToken = accessToken
                nullToken = false
                info("$tempToken PhotoUpload")
            }
        }
    }

}