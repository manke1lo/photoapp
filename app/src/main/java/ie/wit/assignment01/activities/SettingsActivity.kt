package ie.wit.assignment01.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.dropbox.core.android.Auth
import com.dropbox.core.v2.users.FullAccount
import com.google.gson.GsonBuilder
import com.squareup.picasso.Picasso
import ie.wit.assignment01.R
import ie.wit.assignment01.dropbox.DropboxClient
import ie.wit.assignment01.dropbox.UserAccountTask
import ie.wit.assignment01.helpers.read
import ie.wit.assignment01.helpers.write
import kotlinx.android.synthetic.main.activity_settings.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import java.io.File

class SettingsActivity : AppCompatActivity(), AnkoLogger {

    private var accessKey: String? = null
    private lateinit var tempToken: String
    var nullToken: Boolean = true

    val context: Context = this
    val KEY_JSON_FILE = "key.json"
    val gsonbuilder = GsonBuilder().setPrettyPrinting().create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        toolbarAdd.title = "Settings"
        setSupportActionBar(toolbarAdd)

        val filePath = context.filesDir.path.toString() + "/" + KEY_JSON_FILE
        val file = File(filePath)
        if (file.createNewFile()) {
            info("KEY_JSON_FILE has been created")
        }
        else {
            info("KEY_JSON_FILE already exists")
            deserialize()
        }

        if (accessKey != null && accessKey != "null") {
            nullToken = false
            sign_in_button.setText("LOGOUT")
        }

        sign_in_button.setOnClickListener {
            if (nullToken)
                Auth.startOAuth2Authentication(applicationContext, getString(R.string.APP_KEY))
            else {
                logout()
            }
        }

    }

    override fun onResume() {
        super.onResume()
        getAccessToken()
    }

    fun getAccessToken() {
        val accessToken = Auth.getOAuth2Token() //generate Access Token
        if (accessToken != null) {
            info("Acquired Access Key")
            info("$accessToken")
            //Store accessToken in SharedPreferences
            val prefs = getSharedPreferences("com.example.valdio.dropboxintegration", Context.MODE_PRIVATE)
            prefs.edit().putString("access-token", accessToken).apply()
            tempToken = accessToken
            info("temp token $tempToken")
            nullToken = false
            getUserAccount()
            createKey(accessToken)
        }
    }

    //function provided by https://www.sitepoint.com/adding-the-dropbox-api-to-an-android-app/
    private fun getUserAccount() {
        if (nullToken) return
        UserAccountTask(DropboxClient.getClient(tempToken), object : UserAccountTask.TaskDelegate {
            override fun onAccountReceived(account: FullAccount) {
                //Print account's info
                info("account received")
                info("User $account")
                updateUI(account)
            }

            override fun onError(error: Exception) {
                info("Error receiving account details.")
            }
        }).execute()
    }

    private fun updateUI(account: FullAccount) {
        userName.setText(account.name.displayName)
        email.setText(account.email)
        Picasso.with(this)
                .load(account.profilePhotoUrl)
                .resize(200,200)
                .into(profilePic)
        sign_in_button.setText("LOGOUT")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_save, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.saveSettings -> {
                val resultIntent = Intent()
                resultIntent.putExtra("Token", tempToken)
                setResult(RESULT_OK, resultIntent)
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun createKey(token: String) {
        accessKey = token
        serialize()
    }

    fun logout() {
        accessKey = null
        nullToken = true
        sign_in_button.setText("SIGN IN TO DROPBOX")
        serialize()
    }

    private fun serialize() {
        val jsonString = gsonbuilder.toJson(accessKey)
        write(context, KEY_JSON_FILE, jsonString)
    }

    private fun deserialize() {
        val jsonString = read(context, KEY_JSON_FILE)
        accessKey = jsonString
        info("json token = $accessKey")
        if (accessKey != null) {
            tempToken = accessKey!!
            nullToken = false
        }
    }
}
