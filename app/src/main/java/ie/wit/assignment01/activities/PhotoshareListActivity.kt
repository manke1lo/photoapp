package ie.wit.assignment01.activities

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.view.*
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.menu.ListMenuItemView
import androidx.recyclerview.widget.LinearLayoutManager
import ie.wit.assignment01.R
import ie.wit.assignment01.main.MainApp
import ie.wit.assignment01.models.PhotoshareJSONStore
import ie.wit.assignment01.models.PhotoshareModel
import kotlinx.android.synthetic.main.activity_list_photoshare.*
import kotlinx.android.synthetic.main.card_photoshare.*
import org.jetbrains.anko.*
import java.nio.file.Files.delete
import java.text.SimpleDateFormat
import java.util.*

class PhotoshareListActivity : AppCompatActivity(), DeleteListener, PhotoshareListener, AnkoLogger
{
    lateinit var app: MainApp
    //start and end date for each event
    var dateStart = Date()
    var dateEnd = Date()

    private var ACCESS_TOKEN: String? = null
    private val GET_TOKEN = 8945

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_photoshare)
        app = application as MainApp

        toolbar.title = title
        setSupportActionBar(toolbar)

        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        loadEvents()

        createNewEvent.setOnClickListener {
            startActivityForResult(intentFor<PhotoshareActivity>(), 0)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.item_settings ->
                startActivityForResult(intentFor<SettingsActivity>(), GET_TOKEN)
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onDeleteClick(event: PhotoshareModel)
    {
        val mBuilder = AlertDialog.Builder(this)
        mBuilder.setTitle("Delete Event")
        mBuilder.setMessage("Are you sure you want to delete this Event?")

        //creates an onClick listener for pop up "Delete" button
        mBuilder.setPositiveButton("Delete") {dialog, which ->

            app.events.delete(event)
            toast("Event Deleted")
            showevents(app.events.findAll())
        }

        //creates an onClick listener for "Cancel" button
        mBuilder.setNeutralButton("Cancel") {dialog, which ->
            dialog.cancel()
        }
        val mDialog = mBuilder.create()
        mDialog.show()

    }


    override fun onPhotoshareClick(event: PhotoshareModel)
    {
        if (event.start.isNotEmpty() && event.end.isNotEmpty()) {
            //parses the strings in the event start date, start time, end date, and end time text boxes
            //using the pattern day/month/year hour minute
            dateStart = SimpleDateFormat("dd/MM/yyyyHH:mm").parse(event.start + event.startTime)
            dateEnd = SimpleDateFormat("dd/MM/yyyyHH:mm").parse(event.end + event.endTime)
        }
        //date when user clicks the event (includes time)
        var curDate = Date()
        info("current date $curDate")
        info("event start date $dateStart")
        info("event end date $dateEnd")
        //if the current date is within the range of the event dates, the photo upload activity is opened
        //if end date is before start date, the program starts the range at the given end date
        if ((dateEnd.time - dateStart.time) > 0) {
            if (curDate in dateStart..dateEnd) {
                info("event currently happening")
                var uploadIntent = intentFor<PhotoUpload>()
                uploadIntent.putExtra("Token", ACCESS_TOKEN)
                uploadIntent.putExtra("event_edit", event)
                startActivityForResult(uploadIntent, 1)
            }
            else {
                startActivityForResult(intentFor<PhotoshareActivity>().putExtra("event_edit", event), 1)
            }
        }
        else {
            if ((dateEnd.time - dateStart.time) < 0)
                toast("Event ends before start date")
            startActivityForResult(intentFor<PhotoshareActivity>().putExtra("event_edit", event), 1)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        loadEvents()
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK) {
            when(requestCode) {
                GET_TOKEN -> {
                    if (data != null) {
                        ACCESS_TOKEN = data.getStringExtra("Token")
                        info("$ACCESS_TOKEN PhotoshareListActivity")
                    }
                }
            }
        }
    }

    private fun loadEvents() {
        showevents(app.events.findAll())
    }

    fun showevents (events: List<PhotoshareModel>) {
        recyclerView.adapter = PhotoshareAdapter(events, this, this)
        recyclerView.adapter?.notifyDataSetChanged()
    }

}

