package ie.wit.assignment01.dropbox

import com.dropbox.core.v2.DbxClientV2
import com.dropbox.core.DbxRequestConfig

//Original code provided by https://www.sitepoint.com/adding-the-dropbox-api-to-an-android-app/ and converted to Kotlin
object DropboxClient {

    fun getClient(ACCESS_TOKEN: String): DbxClientV2 {
        // Create Dropbox client
        val config = DbxRequestConfig("dropbox/sample-app", "en_US")
        return DbxClientV2(config, ACCESS_TOKEN)
    }
}